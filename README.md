# Mind+阿里云物联网平台JSON解析图形化库

### 介绍

本图形化库是基于Mind+官方支持的阿里云物联网平台MQTT功能的基础上编写阿里云物联网平台JSON解析图形化库，支持多参数并发上报以及数据下发。

### 软件架构
基于Mind+ V1.7.2 RC3.0开发。

### 安装教程

#### 方法1.
利用【 **https://gitee.com/LNSFAIoT/Mindplus-Alibaba-Cloud-IoT-Platform-JSON-parsing-graphical-library.git** 】 **复制** 在Mind+ **用户库** 搜索栏中进行在线下载；

#### 方法2.
选择 **直接下载** 【 **lnsfaiot-aliyunjson-thirdex-V0.0.2.mpext** 】文件，直接 **外部导入** 。

### 使用说明

#### 积木截图

![输入图片说明](image/%E9%98%BF%E9%87%8C%E4%BA%91%E7%89%A9%E8%81%94%E7%BD%91%E5%B9%B3%E5%8F%B0JSON%E7%A7%AF%E6%9C%A8.png)

#### 阿里云物联网平台JSON代码

![输入图片说明](image/%E9%98%BF%E9%87%8C%E4%BA%91%E7%89%A9%E8%81%94%E7%BD%91%E5%B9%B3%E5%8F%B0JSON%E4%BB%A3%E7%A0%81.png)

### 参与贡献

 **岭师人工智能素养教育共同体** 

### 如何加入

 **请关注微信公众号：人工智能素养教育共同体。** 

![输入图片说明](image/%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E7%B4%A0%E5%85%BB%E6%95%99%E8%82%B2%E5%85%B1%E5%90%8C%E4%BD%93%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)

### 捐助
如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。

![输入图片说明](image/%E8%AF%B7%E5%A4%9A%E5%A4%9A%E6%94%AF%E6%8C%81%E6%88%91%E4%BB%AC.jpg)

### 联系我们
有关 **意见反馈** 和 **合作意向** 联系，可扫码添加以下 **企业微信** 。

#### 黄老师：

![输入图片说明](image/%E9%BB%84%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)

#### 曾老师：

![输入图片说明](image/%E6%9B%BE%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)
