# mind+ AliCloud IoT platform JSON parsing graphical library

#### Introduction

This graphical library is based on mind+ official support for the AliCloud IoT platform MQTT functionality based on writing AliCloud IoT platform JSON parsing graphical library, support AliCloud key-value pairs one or more parameters to report and parameters to obtain.

#### software architecture
Based on Mind+ V1.7.2 RC3.0 development.

#### installation tutorial

Method 1. Use [**https://gitee.com/LNSFAIoT/Mindplus-Alibaba-Cloud-IoT-Platform-JSON-parsing-graphical-library.git**] **copy** in mind+ **user library** search field for online download.

Method 2. Choose **direct download** [**lnsfaiot-aliyunjson-thirdex-V0.0.2.mpext**] file and import it directly **externally** .

#### Instructions for use

Screenshot of building blocks

![输入图片说明](image/%E9%98%BF%E9%87%8C%E4%BA%91%E7%89%A9%E8%81%94%E7%BD%91%E5%B9%B3%E5%8F%B0JSON%E7%A7%AF%E6%9C%A8.png)

AliCloud IoT platform JSON code

![输入图片说明](image/%E9%98%BF%E9%87%8C%E4%BA%91%E7%89%A9%E8%81%94%E7%BD%91%E5%B9%B3%E5%8F%B0JSON%E4%BB%A3%E7%A0%81.png)

#### Participating Contributions

 **Lingji Artificial Intelligence Literacy Education Community** 

 **WeChat: ** **Artificial Intelligence Literacy Education Community** 

![输入图片说明](image/%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E7%B4%A0%E5%85%BB%E6%95%99%E8%82%B2%E5%85%B1%E5%90%8C%E4%BD%93%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)

#### Donate
If you find our open source software helpful, please scan the QR code below to reward us with a cup of coffee.

![输入图片说明](image/%E8%AF%B7%E5%A4%9A%E5%A4%9A%E6%94%AF%E6%8C%81%E6%88%91%E4%BB%AC.jpg)

#### Contact Us
For **Feedback** and **Cooperation Intention** contact, you can scan the code to add the following **Corporate WeChat**.

Ms. Huang.

![输入图片说明](image/%E9%BB%84%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)

Mr. Zeng.

![输入图片说明](image/%E6%9B%BE%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)